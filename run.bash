#!/usr/bin/env bash

if [ ! -d "env" ]
then
echo -e "\n*** Virtual environment not found, installing\n"
python -m venv env
source ./env/bin/activate
pip install mkdocs
fi

echo -e "\n*** Virtual environment found in ./env\n"
echo -e "*** Serving documentation website locally, press Ctrl+C to stop it\n"
mkdocs serve
