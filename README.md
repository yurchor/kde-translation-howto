# KDE Translation How-To

Draft of the manual for KDE translators.

Current goals:

* The translation howto should be task-oriented
* It should be concise.
* It should address the following topics
  * “I want to start translating language foo”
  * “I’ve tried contacting language team foo but no answer”
     when a team exist, when a team doesn’t exist (or doesn’t exist anymore)
  * “I’ve got translation access, how do I move forward”
    clear information on where translations can be found and the meaning of branches, and how to update and commit.
  * Advanced information are fine, but should not hinder the comprehension of simple tasks

## Build

After cloning the repository:

```
cd kde-translation-howto
mkdir build
cd build
meinproc5 ../translation-howto.docbook
```

To check the resulting web page with the KDE style, open it in KHelpCenter.

If you want to check all of the docbook's content at once without needing to click at urls, run:

```bash
meinproc5 ../translation-howto.docbook --output index.html
```

One step further, if you're using Kate to edit this docbook, here are four tips:

* Configure Kate to use 2 spaces for indentation

Settings > Configure Kate... > Editing > Indentation > Indentation width: 2 characters

* Select a misaligned section and realign it properly

Select text to be aligned > Tools > Align (avoid the option Clean indentation)

* Create an external tool to compile the docbook then add it to the toolbar

Settings > Configure Kate... > External Tools > Add > Add tool... > Executable: meinproc5 > Command: %{Document:FileName} --output build/index.html > Right click the Kate toolbar > Configure Toolbars... > Main Toolbar `<externaltools>` > Add your external tool

* Instead of manually adding multiple paragraphs, select multiple lines then use Ctrl + Alt + Down to duplicate blocks of text and Ctrl + Shift + Up/Down to move them to where you want them

## View on a webpage

The How-To is being rewritten in markdown for ease of writing. See [how to preview it live](howtoinmarkdown/README.md).

## License

This project is licensed under FDL-1.3-or-later.
