# Translation HowTo

Look at howtoinmarkdown/howtostructure.md directly for a better grasp of this page.

# Flow

# Quick Start
## Download
### Download a single file
### Download a folder
## Translate
## Deliver

# New Translators
## Contact the Teams
## Download
## Translate
## Deliver

# Experienced Translators
## Translation access
## Downloading SVN
## Translate
## Deliver

# New Teams
## Contact main i18n team
## Translation access
## Downloading SVN
## Translate
## Deliver

# Branches
## Stable
## Trunk

# Personal Info

# Interface
## Segments
## Source
## Target
## Notes
## Alternative translations

# Lokalize Workflow
## Moving through segments
## Terms
## Tags
## Accelerators
## Plurals
## Text-specific
### Interface
### Documentation
### Websites
## Tips and tricks

# Manually
## Mailing list
## Direct email
## Group chat

# SVN
## Linux
### Kdesvn
### Dolphin SVN plugin
### Manual SVN commands
## Windows
### TortoiseSVN
### Manual SVN commands
## BSD
### Kdesvn
### Manual SVN commands
