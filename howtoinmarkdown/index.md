This is the repository for the new KDE Translation How-To.

To run it locally in a webpage, run:

```bash
git clone https://invent.kde.org/yurchor/kde-translation-howto.git
cd kde-translation-howto
chmod +x run.bash
./run.bash
```

Then you only need to run the script every time you want to start serving the website again.
